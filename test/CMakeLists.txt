find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})
 
add_executable(tests )
target_link_libraries(tests ${GTEST_BOTH_LIBRARIES} pthread)

add_custom_target(run_tests
    COMMAND ./tests
    DEPENDS tests
)
